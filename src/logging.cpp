#include "logging.hpp"

void log_suffix(Print* output) {
  output->print("\n");
}

void log_prefix(Print* output) {
  char c[30];
  tm now = get_time_tm();

  // Simple time-set detection
  if(now.tm_year > 70) {
    strftime(c, sizeof(c), "[%Y-%m-%d %r] ", &now);
    output->print(c);
  } else {
    sprintf(c, "[%06ld]", millis());
    output->print(c);
  }
}

void logging_setup() {
  Serial.begin(115200);
  Serial.println();
  Log.begin(LOG_LEVEL_VERBOSE, &Serial);
  Log.setPrefix(log_prefix);
  Log.setSuffix(log_suffix);
};
