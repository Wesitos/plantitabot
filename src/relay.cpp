#include "relay.hpp"

void Relay::setup() {
  pinMode(pin, OUTPUT);
  set(state);
}

void Relay::set(int new_state) {
  Log.verbose("[Relay(%d)] Set value: %T", pin, new_state);
  state = new_state;
  digitalWrite(pin, state ^ normal_state ^ active_value);
}

void Relay::on() {
  if (state) return;
  Log.notice("[Relay(%d)] Turn on", pin);
  set(true);
}

void Relay::off() {
  if (!state) return;
  Log.notice("[Relay(%d)] Turn off", pin);
  set(false);
}


