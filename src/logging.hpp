/**
 * Loggeo a puerto serial e intento de loggeo a servidor TCP
 */
#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>

#include "time.hpp"
#include "wifi.hpp"

void logging_setup();
