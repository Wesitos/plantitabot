#include "config.hpp"

bool load_config(Config& config, const char* filename) {
  SPIFFS.begin();

  if(!SPIFFS.exists(filename)) {
    Log.fatal("[Config] Config file does not exist");
    return false;
  }

  File configFile = SPIFFS.open(filename, "r");
  if (!configFile) {
    Log.fatal("[Config] Failed to open config file");
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    Log.fatal("[Config] Config file size is too large.");
    return false;
  }

  DynamicJsonBuffer buffer(400);

  JsonObject& json = buffer.parseObject(configFile);
  configFile.close();

  SPIFFS.end();

  if (!json.success()) {
    Log.fatal("[Config] Failed to parse config file");
    return false;
  }

  Log.notice("[Config] Config file loaded");
  Log.verbose("[Config] Config file deserialized size: %d bytes", buffer.size());
  init_config_struct(config, json);

  return true;
}

void init_config_struct(Config& config, JsonObject& json) {
  strlcpy(config.hostname, json["hostname"] | "", sizeof(config.hostname));

  JsonArray& wifi = json["wifi"];

  for (int i=0; i < wifi.size(); i++) {
    WifiNetwork& network = config.wifi.networks[i];
    strlcpy(network.ssid,
            wifi[i]["ssid"], sizeof(network.ssid));
    strlcpy(network.password,
            wifi[i]["password"], sizeof(network.password));
  }
  config.wifi.num_networks = wifi.size();

  if (json["light"]["duration"]) {
    config.light.duration = json["light"]["duration"];
  } else {
    config.light.duration = 8;
  }

  if (json["time"]["zone"]) {
    config.time.zone = json["time"]["zone"];
  } else {
    config.time.zone = -5;
  }

  Log.verbose("[Config] Loaded \"hostname\": \"%s\"", config.hostname);
  for (int i=0; i< config.wifi.num_networks; i++) {
    Log.verbose("[Config] Loaded Wifi SSID: \"%s\"", config.wifi.networks[i].ssid);
  }
  Log.verbose("[Config] Loaded \"light.duration\": %F", config.light.duration);
}
