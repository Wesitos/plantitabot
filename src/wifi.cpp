#include "wifi.hpp"

ESP8266WiFiMulti wifiMulti;

WiFiEventHandler station_connected_handler;
WiFiEventHandler station_disconnected_handler;
WiFiEventHandler station_got_ip_handler;

WiFiServer LoggingServer(23);


void on_station_connected(const WiFiEventStationModeConnected& event) {
  Log.notice("[WiFi] Connected to \"%s\"", event.ssid.c_str());
}

void on_station_disconnected(const WiFiEventStationModeDisconnected& event) {
  Log.notice("[WiFi] Disconnected from \"%s\"", event.ssid.c_str());
}

void on_station_got_ip(const WiFiEventStationModeGotIP& event) {
  Log.notice("[WiFi] IP address: %s", event.ip.toString().c_str());
}

void wifi_setup(Config config) {
  // Avoid writing the WIFI SSID and password to flash memory
  // to avoid early memory wearing
  WiFi.persistent(false);
  WiFi.hostname(config.hostname);
  WiFi.mode(WIFI_STA);
  wifi_set_sleep_type(LIGHT_SLEEP_T);

  for (int i=0; i < config.wifi.num_networks; i++) {
    WifiNetwork network = config.wifi.networks[i];
    wifiMulti.addAP(network.ssid, network.password);
  }
  Log.notice("[WiFi] Connecting Wifi");

  station_connected_handler = WiFi.onStationModeConnected(&on_station_connected);
  station_disconnected_handler =
    WiFi.onStationModeDisconnected(&on_station_disconnected);
  station_got_ip_handler = WiFi.onStationModeGotIP(&on_station_got_ip);

  wifi_loop();
}

bool wifi_loop() {
  static bool connected = false;
  static unsigned long last_time = 0;

  wl_status_t status = wifiMulti.run();

  if (status == WL_CONNECTED && connected == false) {
    connected = true;
  }

  if (status != WL_CONNECTED && millis() > last_time + 5000) {
    Log.warning("[WiFi] Is not connected");
    Log.warning("[WiFi] Trying to connect to \"%s\"", WiFi.SSID().c_str());
    connected = false;
    last_time = millis();
  }
  return connected;
}

bool is_network_ready() {
  return wifi_loop();
}
