/**
 * Clase reutilizable para controlar un Relay
 */

#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>

#define NORMALLY_OPEN true
#define NORMALLY_CLOSED false

class Relay {
  int pin;
  bool active_value; /**< Valor logico para el cual el relé se encuentra activo */
  bool normal_state; /**< verdadero si el relé se encuentra abierto por defecto */
  bool state;
public:
  Relay(int pin, bool active_value=HIGH, bool normal_state=NORMALLY_OPEN,
        bool initial_state=false):
    pin(pin),
    active_value(active_value),
    normal_state(normal_state),
    state(initial_state)
  {};
  void setup();
  void set(int new_state);
  void on();
  void off();
  bool get() { return state; }
};

