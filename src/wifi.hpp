/**
 * Conexión a red WiFi
 */
#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include "config.hpp"

void wifi_setup(Config config);
bool wifi_loop();
bool is_network_ready();

extern WiFiServer LoggingServer;
