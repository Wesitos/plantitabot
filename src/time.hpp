/**
 * Configuración de reloj de software y obtención de tiempo desde internet
 */
#pragma once

#include <time.h>
#include <Arduino.h>
#include <ArduinoLog.h>

#define SNTP_UPDATE_DELAY 1800000

#ifndef NTP_SERVER_1
#define NTP_SERVER_1 "0.south-america.pool.ntp.org"
#endif

#ifndef NTP_SERVER_2
#define NTP_SERVER_2 "1.south-america.pool.ntp.org"
#endif

#ifndef NTP_SERVER_3
#define NTP_SERVER_3 "2.south-america.pool.ntp.org"
#endif

#ifndef TIMEZONE
#define TIMEZONE -5
#endif

bool time_setup();
tm get_time_tm();
void print_timestamp(Print* _logOutput);

extern bool is_network_ready();

// Gets the time of the day as a number of hours
float tm_to_float(tm time);
