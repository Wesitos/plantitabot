/**
 * Logica de control
 */

#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>

#include "config.hpp"
#include "relay.hpp"
#include "time.hpp"

#define LIGHT_PIN D7

extern Relay LightRelay;

/**
 * Inicializa la configuración de control
 * @param config Configuración
 */
void control_setup(Config& config);

/**
 * Bucle de la logica de control
 */
void control_loop();

/**
 * Bucle del control de la lampara
 * @param now Hora actual
 */
void light_loop(const tm now);
