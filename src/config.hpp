/**
 * Lectura de configuración desde el sistema de archivos SPIFFS
 */
#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>
#include <ArduinoJson.h>
#include <FS.h>

/**
 * Configuracion de la red WiFi a la cual conectarse
 */

struct WifiNetwork {
  char ssid[30];     /**< Nombre de la red*/
  char password[65]; /**< Contraseña de la red*/
};

struct WifiConfig {
  WifiNetwork networks[5];
  int num_networks;
};

/**
 * Configuración de la lampara
 */
typedef struct {
  float duration; /**< Cantidad de horas al dia que debe mantenerse encendida la lampara*/
} LigthConfig;

/**
 * Configuracion de tiempo
 */
typedef struct {
  float zone; /**< Zona horaria (numero de horas respecto a UTC)*/
} TimeConfig;

/**
 * Estructura que almacena la configuración
 */
typedef struct {
  WifiConfig wifi;
  char hostname[20];
  LigthConfig light;
  TimeConfig time;
} Config;

/**
 * Lee la configuración desde el sistema de archivo
 *
 * @param config Estructura de configuración donde almacenar los valores leidos
 * @param filename Nombre del archivo de configuración
 */
bool load_config(Config& config, const char* filename);

/**
 * Vuelca la información de un JsonObject en una estructura de configuración
 */
void init_config_struct(Config& config, JsonObject& json);
