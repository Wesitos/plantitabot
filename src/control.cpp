#include "control.hpp"

Relay LightRelay(LIGHT_PIN);

float light_duration;

void control_setup(Config& config) {
  light_duration = config.light.duration;
  LightRelay.setup();
  light_loop(get_time_tm());
}

void control_loop() {
  static long last_time = millis();
  long now_millis = millis();

  if (now_millis - last_time > 1000) {
    tm now = tm(get_time_tm());
    light_loop(now);
    last_time = now_millis;
  }
}

void light_loop(const tm now) {
  float start_time = 6;
  float end_time = start_time + light_duration;

  if (light_duration >= 24) {
    return LightRelay.on();
  }

  if (light_duration <= 0) {
    return LightRelay.off();
  }

  if (tm_to_float(now) > start_time && tm_to_float(now) < end_time) {
    return LightRelay.on();
  }

  LightRelay.off();

}
