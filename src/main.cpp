#include "main.hpp"

Config config;

void setup() {
  logging_setup();
  Log.notice("[Main] Booting");

  // Load config
  if (!load_config(config, "/config.json")) {
    Log.notice("[Main] Rebooting");
    delay(5000);
    ESP.restart();
  };

  // Network setup
  wifi_setup(config);
  // Network stuff setup
  ota_setup(config);
  time_setup();
  // Turn on builtin led
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  // Hardware setup
  control_setup(config);
}

void loop() {
  wifi_loop();
  ota_loop();
  control_loop();
  health_loop();
  delay(100);
}
