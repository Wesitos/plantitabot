#include "time.hpp"

int timezone;

bool wait_for_time();

bool time_setup() {
  Log.notice("[Time] Waiting for network");
  while (!is_network_ready()) {
    yield();
  }
  configTime(TIMEZONE * 3600, 0, NTP_SERVER_1, NTP_SERVER_2, NTP_SERVER_3);
  Log.notice("[Time] Getting time from timeserver");
  if (!wait_for_time()) {
    return false;
  }
  Log.notice("[Time] Ready");
  return true;
}

bool wait_for_time() {
  for(int i=0; i<21; i++) {
    if (time(NULL) < 100000) {
      Log.verbose("[Time] Waiting for timeserver [%d times]", i + 1);
      delay(200*(i+1));
    }
    else {
      return true;
    }
  }
  Log.error("[Time] Timeserver takes too long to respond.");
  Log.error("[Time] SNTP unavailable");
  return false;
}

tm get_time_tm() {
  time_t now = time(nullptr);
  tm now_tm = *localtime(&now);
  return now_tm;
}

/**
 * Converts tm struct to a float between 0 and 24
 * that represents  the time of the day
 */
float tm_to_float(tm time) {
  return time.tm_hour + time.tm_min / 60.0 + time.tm_sec / 3600;
}

void print_timestamp(Print* _logOutput) {
  char c[30];
  tm now = get_time_tm();
  strftime(c, sizeof(c), "%Y-%m-%d %R ", &now);
  _logOutput->print(c);
}
