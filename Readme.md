# PlantitaBot

Automatización del control de una lampara para crecer plantas usando Arduino y ESP8266

# Hardware
+ Relay de control de la lampara en D0 (GPIO 16)

# Instalación

```bash
  # Realizar la carga por puerto serial
  $ pio run -t upload
  # Realizar la carga por ArduinoOTA (requiere poder recibir peticiones en el puerto 8266)
  $ pio run -e production -t upload
```

# Configuración
El programa lee el archivo `config.json` desde el sistema de archivos de la memoria flash
```json
{
  "hostname": "faraday",
  "wifi": {
    "ssid": "something",
    "password": "some-password"
  },
  "light": {
    "duration": 16
  },
  "time": {
    "zone": -5
  }
}

```

+ **hostname**: Nombre del dispositivo con el que sera visible en la red (rDNS)
+ **wifi**: Configuracion de wifi. Necesario para obtener la hora desde internet
  - wifi.**ssid**: Nombre de la red WiFi a la cual conectarse
  - wifi.**password**: Contraseña de la red WiFi a la cual conectarse.
+ **light**: Configuración de iluminación
  - light.**duration**: Numero de horas al dia que la lampara se debe mantener encendida
+ **time**: Configuracion de tiempo
  - time.**zone**: Zona horaria (Horas respecto a UTC)

Para cargar la configuración desde la carpeta /data
```bash
  # Realizar la carga por puerto serial
  $ pio run -t uploadfs
  # Realizar la carga por ArduinoOTA
  $ pio run -e production -t uploadfs
```
